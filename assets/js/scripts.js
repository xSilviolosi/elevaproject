(function ($) {
    if ($.eleva === undefined) {
        $.eleva = {
            init: function () {
                this.initMap();
                this.bindEvents();
                this.user();
                this.structure();
            },
            bindEvents: function () {
                var self = this;
                // Add active class on menu
                var path = window.location.pathname.split("/").pop() || 'home.html';
                var target = $('nav a[href="' + path + '"]');
                target.addClass('active');
                // Add JQuery-UI Datepicker widget
                $(document).find('#datepicker').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        yearRange: 'c-80:c+10'
                    },
                    $.datepicker.regional['it']
                );
                // Clean url from the parameters and fade out alert message
                if ($(document).find('.alert')) {
                    self.cleanUrl();
                    $(document).find('.alert').delay(5000).fadeOut();
                }
                // Function to create all DB by button in Homepage
                $(document).find('.create-db').on('click', function (e) {
                    e.preventDefault();
                    params = {
                        "section": "other",
                        "method": "createDB"
                    };
                    $.ajax({
                        type: 'GET',
                        cache: false,
                        data: params,
                        url: 'includes/actions.php',
                        dataType: 'html',
                        success: function (data) {
                            $(document).find('.output').html(data);
                        },
                        error: function (request, state, errors) {
                            console.error('Error creating DB: ' + errors);
                        }
                    });
                });
            },
            user: function () {
                var self = this,
                    $form = $(document).find("form[name='frm_user']"),
                    $userList = $(document).find('.user-list');
                //Click new submit
                $form.on('click', '.new-submit', function (e) {
                    e.preventDefault();
                    var params = [],
                        msg = '',
                        error = false,
                        $name = $form.find("input[name='name']"),
                        $surname = $form.find("input[name='surname']"),
                        $email = $form.find("input[name='email']"),
                        $date = $form.find('#datepicker'),
                        $role = $form.find('#role'),
                        $structures = $form.find('#structures');
                    params.push($name, $surname, $email, $date, $role, $structures);
                    $.each(params, function (index, value) {
                        var label = $("label[for='" + value.attr('id') + "']");
                        label.removeClass('text-danger');
                        if (!value.val() || value.val().length === 0) {
                            label.addClass('text-danger');
                            msg = 'Campo ' + label.html() + ' non valorizzato';
                            $(value.attr('id')).focus();
                            error = true;
                            return false;
                        }
                    });
                    if ($email.val() && self.validateEmail($email.val()) === false) {
                        error = true;
                        msg = 'Email non valida';
                    }
                    if (error === true) {
                        alert(msg);
                    } else {
                        $form.attr('action', 'includes/actions.php?section=user&method=add');
                        $form.submit();
                    }
                });
                //Click edit submit
                $form.on('click', '.edit-submit', function (e) {
                    e.preventDefault();
                    var params = [],
                        msg = '',
                        error = false,
                        $name = $form.find("input[name='name']"),
                        $surname = $form.find("input[name='surname']"),
                        $email = $form.find("input[name='email']"),
                        $date = $form.find('#datepicker'),
                        $role = $form.find('#role'),
                        $structures = $form.find('#structures'),
                        idUser = $(this).data('user');
                    params.push($name, $surname, $email, $date, $role, $structures);
                    $.each(params, function (index, value) {
                        var label = $("label[for='" + value.attr('id') + "']");
                        label.removeClass('text-danger');
                        if (!value.val() || value.val().length === 0) {
                            label.addClass('text-danger');
                            msg = 'Campo ' + label.html() + ' non valorizzato';
                            $(value.attr('id')).focus();
                            error = true;
                            return false;
                        }
                    });
                    if ($email.val() && self.validateEmail($email.val()) === false) {
                        error = true;
                        msg = 'Email non valida';
                    }
                    if (error === true) {
                        alert(msg);
                    } else {
                        $form.attr('action', 'includes/actions.php?section=user&method=edit&idUser=' + idUser);
                        $form.submit();
                    }
                });
                //Click cancel edit user
                $form.on('click', '.cancel', function (e) {
                    e.preventDefault();
                    $form.find('.new-submit').removeClass('hide');
                    $form.find('.edit-mode').addClass('hide');
                    self.clearForm($form);
                });
                //Click edit user
                $userList.on('click', '.edit', function (e) {
                    e.preventDefault();
                    var $idUser = $(this).attr('rel');
                    $form.find('.new-submit').addClass('hide');
                    $form.find('.edit-mode').removeClass('hide');
                    self.clearForm($form);
                    // Load data by idUser
                    params = {
                        "section": "user",
                        "method": "loadUser",
                        "idUser": $idUser
                    };
                    $.ajax({
                        type: 'GET',
                        cache: false,
                        data: params,
                        url: 'includes/actions.php',
                        dataType: 'json',
                        success: function (user) {
                            var $name = $form.find("input[name='name']"),
                                $surname = $form.find("input[name='surname']"),
                                $email = $form.find("input[name='email']"),
                                $date = $form.find('#datepicker'),
                                $role = $form.find('#role'),
                                $editBtn = $form.find('.edit-submit');
                            $name.val(user.name);
                            $surname.val(user.surname);
                            $email.val(user.email);
                            $date.val(user.birthdate);
                            $role.find('option[value="' + user.role + '"]').prop('selected', true);
                            $editBtn.data('user', user.user_id);
                            $.each(user.structures[0], function (i, e) {
                                $('#structures option[value="' + e.structure_id + '"]').prop('selected', true);
                            });
                            console.log(user);
                        },
                        error: function (request, state, errors) {
                            console.error('Error loading user: ' + errors);
                        }
                    });
                });
            },
            structure: function () {
                var self = this,
                    $form = $(document).find("form[name='frm_structure']"),
                    $structureList = $(document).find('.structure-list');
                //Click new submit
                $form.on('click', '.new-submit', function (e) {
                    e.preventDefault();
                    var params = [],
                        msg = '',
                        error = false,
                        $name = $form.find("input[name='name']"),
                        $address = $form.find("input[name='address']"),
                        $lat = $form.find("input[name='lat']"),
                        $lon = $form.find("input[name='lon']");
                    params.push($name, $address, $lat, $lon);
                    $.each(params, function (index, value) {
                        var label = $("label[for='" + value.attr('id') + "']");
                        label.removeClass('text-danger');
                        if (!value.val()) {
                            label.addClass('text-danger');
                            msg = 'Campo ' + label.html() + ' non valorizzato';
                            $(value.attr('id')).focus();
                            error = true;
                            return false;
                        }
                    });
                    if (error === true) {
                        alert(msg);
                    } else {
                        $form.attr('action', 'includes/actions.php?section=structure&method=add');
                        $form.submit();
                    }
                });
                //Click edit submit
                $form.on('click', '.edit-submit', function (e) {
                    e.preventDefault();
                    var params = [],
                        msg = '',
                        error = false,
                        $name = $form.find("input[name='name']"),
                        $address = $form.find("input[name='address']"),
                        $lat = $form.find("input[name='lat']"),
                        $lon = $form.find("input[name='lon']"),
                        idStructure = $(this).data('structure');
                    params.push($name, $address, $lat, $lon);
                    $.each(params, function (index, value) {
                        var label = $("label[for='" + value.attr('id') + "']");
                        label.removeClass('text-danger');
                        if (!value.val()) {
                            label.addClass('text-danger');
                            msg = 'Campo ' + label.html() + ' non valorizzato';
                            $(value.attr('id')).focus();
                            error = true;
                            return false;
                        }
                    });
                    if (error === true) {
                        alert(msg);
                    } else {
                        $form.attr('action', 'includes/actions.php?section=structure&method=edit&idStructure=' + idStructure);
                        $form.submit();
                    }
                });
                //Click cancel edit structure
                $form.on('click', '.cancel', function (e) {
                    e.preventDefault();
                    $form.find('.new-submit').removeClass('hide');
                    $form.find('.edit-mode').addClass('hide');
                    self.clearForm($form);
                });
                //Click edit structure
                $structureList.on('click', '.edit', function (e) {
                    e.preventDefault();
                    var $idStructure = $(this).attr('rel');
                    $form.find('.new-submit').addClass('hide');
                    $form.find('.edit-mode').removeClass('hide');
                    self.clearForm($form);
                    // Load data by idUser
                    params = {
                        "section": "structure",
                        "method": "loadStructure",
                        "idStructure": $idStructure
                    };
                    $.ajax({
                        type: 'GET',
                        cache: false,
                        data: params,
                        url: 'includes/actions.php',
                        dataType: 'json',
                        success: function (structure) {
                            var $name = $form.find("input[name='name']"),
                                $address = $form.find("input[name='address']"),
                                $lat = $form.find("input[name='lat']"),
                                $lon = $form.find("input[name='lon']"),
                                $editBtn = $form.find('.edit-submit');
                            $name.val(structure.name);
                            $address.val(structure.address);
                            $lat.val(structure.lat);
                            $lon.val(structure.lon);
                            $editBtn.data('structure', structure.structure_id);
                            console.log(structure);
                        },
                        error: function (request, state, errors) {
                            console.error('Error loading structure: ' + errors);
                        }
                    });
                });
            },
            initMap: function () {
                var self = this;
                if ($(document).find('#map').length > 0) {
                    var map,
                        section = $(document).find('#map').data('section'),
                        $sidebar = $(document).find('#sidebar'),
                        gMaps = google.maps,
                        geocoder = new gMaps.Geocoder(),
                        elevaPosition = {lat: 45.140002, lng: 9.120266};
                    // Set default maps focus
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: elevaPosition,
                        zoom: 17
                    });
                    // Set default marker
                    var marker = new google.maps.Marker({
                        position: elevaPosition,
                        map: map
                    });
                    switch (section) {
                        case 'structures':
                            var $form = $(document).find("form[name='frm_structure']"),
                                $address = $form.find("input[name='address']"),
                                address = document.getElementById('address'),
                                $lat = $form.find("input[name='lat']"),
                                $lon = $form.find("input[name='lon']");
                            // Get coordinates from click and set marker
                            gMaps.event.addListener(map, 'click', function (event) {
                                var lat = event.latLng.lat(),
                                    lng = event.latLng.lng();
                                geocoder.geocode({
                                    'latLng': event.latLng
                                }, function (results, status) {
                                    if (status == gMaps.GeocoderStatus.OK) {
                                        if (results[0]) {
                                            $address.val(results[0].formatted_address);
                                            marker.setPosition(results[0].geometry.location);
                                            marker.setVisible(true);
                                        }
                                    }
                                });
                                $lat.val(lat);
                                $lon.val(lng);
                            });
                            // Initialize Places Autocomplete
                            var autocomplete = new gMaps.places.Autocomplete(address);
                            autocomplete.bindTo('bounds', map);
                            autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
                            autocomplete.addListener('place_changed', function () {
                                marker.setVisible(false);
                                var place = autocomplete.getPlace();
                                if (!place.geometry) {
                                    window.alert("Nessun dettaglio trovato per l'input: '" + place.name + "'");
                                    return;
                                }
                                // Set position from Autocomplete
                                if (place.geometry.viewport) {
                                    map.fitBounds(place.geometry.viewport);
                                } else {
                                    map.setCenter(place.geometry.location);
                                    map.setZoom(17);
                                }
                                marker.setPosition(place.geometry.location);
                                marker.setVisible(true);
                                // Fill lat lon inputs
                                $lat.val(autocomplete.getPlace().geometry.location.lat());
                                $lon.val(autocomplete.getPlace().geometry.location.lng());
                            });
                            break;
                        case 'map':
                            var myLatlng,
                                info = '',
                                activeInfoWindow,
                                markers = [],
                                bounds = new gMaps.LatLngBounds();
                            marker.setMap(null);
                            // Load markers from DB
                            params = {
                                "section": "map",
                                "method": "loadStructures"
                            };
                            $.ajax({
                                type: 'GET',
                                cache: false,
                                data: params,
                                url: 'includes/actions.php',
                                dataType: 'json',
                                success: function (structures) {
                                    $.each(structures, function (index, value) {
                                        if (value.lat && value.lon) {
                                            myLatlng = new gMaps.LatLng(value.lat, value.lon);
                                            // Create marker
                                            var marker = new gMaps.Marker({
                                                position: myLatlng,
                                                title: value.name
                                            });
                                            // Create infoWindow
                                            info =
                                                '<div class="info">' +
                                                '<br />' +
                                                '<h3>' + value.name + '</h3>' +
                                                '<p>' + value.address + '</p>' +
                                                '<hr>' +
                                                '<h6>Personale:</h6>';
                                            // Load users per structure
                                            for (var i = 0; i < value.users[0].length; i++) {
                                                info += '<span>&bull;&nbsp;' + value.users[0][i].name + '&nbsp;' + value.users[0][i].surname + '&nbsp;-&nbsp;' + value.users[0][i].role + '</span><br />';
                                            }
                                            info += '</div>';
                                            var infoWindow = new gMaps.InfoWindow({
                                                content: info
                                            });
                                            // Set click Event to open infoWindow
                                            marker.addListener('click', function () {
                                                $sidebar.find('.info').html("");
                                                // Check if an infoWindow is opened from another marker before
                                                if (activeInfoWindow) activeInfoWindow.close();
                                                infoWindow.open(map, marker);
                                                activeInfoWindow = infoWindow;
                                                $sidebar.find('.info').html(activeInfoWindow.content);
                                                $sidebar.addClass('reveal');
                                            });
                                            // Method to close sidebar on close marker
                                            gMaps.event.addListener(infoWindow, 'closeclick', function () {
                                                self.closeSidebar($sidebar);
                                            });
                                            markers.push(marker);
                                        }
                                        // Resize maps focus
                                        $.each(markers, function (index, value) {
                                            value.setMap(map);
                                            bounds.extend(value.getPosition());
                                        })
                                    });
                                    map.fitBounds(bounds);
                                },
                                error: function (request, state, errors) {
                                    console.error('Error loading structures: ' + errors);
                                }
                            });
                            break;
                    }
                    $(document).on('click', '.close-sidebar', function (e) {
                        e.preventDefault();
                        self.closeSidebar($sidebar);
                    })
                }
            },
            validateEmail: function (email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            },
            cleanUrl: function () {
                var uri = window.location.toString();
                if (uri.indexOf('?') > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf('?'));
                    window.history.replaceState({}, document.title, clean_uri);
                }
            },
            clearForm: function ($form) {
                $form.find('input[type="text"],select').val("");
                $form.find('#structures option:selected').removeAttr('selected');
            },
            closeSidebar: function ($sidebar) {
                if ($sidebar.hasClass('reveal')) $sidebar.removeClass('reveal');
            }
        }
    }
})(jQuery);

$(document).ready(function () {
    $.eleva.init();
});