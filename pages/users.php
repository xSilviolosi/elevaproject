<?php

class createPage {

    static function page() {
        global $mysqli;

        if (isset($_POST)) extract($_POST);
	    if (isset($_GET)) extract($_GET);
        ?>
        <div class="row margin-top-bottom-21">
            <div class="col-12">
                <h1>Gestione personale</h1>
                <hr>
                <?php if ($section && $method && $msg) Utility::getAlertMsg($section, $method, $msg); ?>
            </div>
            <div class="col-md-5">
                <h2>Inserimento nuovo personale:</h2>
                <form action="users.html" method="POST" name="frm_user">
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="surname">Cognome</label>
                        <input type="text" name="surname" class="form-control" id="surname" placeholder="Cognome">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" class="form-control" id="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="datepicker">Data di nascita</label>
                        <input name="datepicker" id="datepicker" class="form-control" placeholder="gg/mm/aaaa" readonly>
                    </div>
                    <div class="form-group">
                        <label for="role">Ruolo</label>
                        <select name="role" class="form-control" id="role">
                            <option value="addetto">Addetto</option>
                            <option value="supervisore">Supervisore</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="structures">Impianto (puoi selezionarne pi&ugrave; di uno)</label>
                        <?php
                        $result = $mysqli->query("SELECT * FROM structures") or die($mysqli->error);
                        if (mysqli_num_rows($result) > 0) {
                            ?>
                            <select name="structures[]" class="form-control" id="structures" multiple>
                                <?php
                                while ($row = $result->fetch_assoc()) {
                                    ?>
                                    <option value="<?= $row['structure_id'] ?>"><?= $row['name'] ?>
                                        : <?= $row['address'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <?php
                        } else {
                            ?>
                            <a href="structures.html">Nessun impianto trovato. Inseriscine uno.</a>
                            <?php
                        }
                        ?>
                    </div>
                    <button class="new-submit btn btn-primary margin-top-bottom-21">Inserisci personale</button>
                    <div class="edit-mode hide margin-top-bottom-21">
                        <button class="edit-submit btn btn-warning">Modifica personale</button>
                        <a href="javascript:void(0);" class="cancel">Annulla</a>
                    </div>
                </form>
            </div>
            <div class="col-md-6 offset-md-1">
                <h2>Lista persone:</h2>
                <div class="table-responsive">
                    <table class="table table-striped user-list">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Cognome</th>
                            <th>Email</th>
                            <th class="text-center">Modifica</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $result = $mysqli->query("SELECT * FROM users") or die($mysqli->error);
                        if (mysqli_num_rows($result) > 0) {
                            while ($row = $result->fetch_assoc()) {
                                ?>
                                <tr>
                                    <td><?= $row['name'] ?></td>
                                    <td><?= $row['surname'] ?></td>
                                    <td><?= $row['email'] ?></td>
                                    <td class="text-center">
                                        <a href="javascript:void(0);"
                                           class="edit"
                                           rel="<?= $row['user_id'] ?>">Modifica</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="4">Lista personale vuota</td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

}

?>