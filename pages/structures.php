<?php

class createPage {

    static function page() {
        global $mysqli;

	    if (isset($_POST)) extract($_POST);
	    if (isset($_GET)) extract($_GET);
        ?>
        <div class="row margin-top-bottom-21">
            <div class="col-12">
                <h1>Gestione impianti</h1>
                <hr>
                <?php if ($section && $method && $msg) Utility::getAlertMsg($section, $method, $msg); ?>
            </div>
            <div class="col-md-5">
                <h2>Inserimento nuovo impianto:</h2>
                <form action="structures.html" method="POST" name="frm_structure">
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" class="form-control" id="address" placeholder="Address">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label for="lat">Latitudine</label>
                            <input type="text" name="lat" class="form-control" id="lat" placeholder="Latitudine"
                                   readonly>
                        </div>
                        <div class="form-group col-6">
                            <label for="lon">Longitudine</label>
                            <input type="text" name="lon" class="form-control" id="lon" placeholder="Longitudine"
                                   readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-12">
                            <div id="map" data-section="structures"></div>
                        </div>
                    </div>
                    <button class="new-submit btn btn-primary margin-top-bottom-21">Inserisci impianto</button>
                    <div class="edit-mode hide margin-top-bottom-21">
                        <button class="edit-submit btn btn-warning">Modifica impianto</button>
                        <a href="javascript:void(0);" class="cancel">Annulla</a>
                    </div>
                </form>
            </div>
            <div class="col-md-6 offset-md-1">
                <h2>Lista impianti:</h2>
                <div class="table-responsive">
                    <table class="table table-striped structure-list">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Indirizzo</th>
                            <th class="text-center">Modifica</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $result = $mysqli->query("SELECT * FROM structures") or die($mysqli->error);
                        if (mysqli_num_rows($result) > 0) {
                            while ($row = $result->fetch_assoc()) {
                                ?>
                                <tr>
                                    <td><?= $row['name'] ?></td>
                                    <td><?= $row['address'] ?></td>
                                    <td class="text-center">
                                        <a href="javascript:void(0);"
                                           class="edit"
                                           rel="<?= $row['structure_id'] ?>">Modifica</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="4">Lista impianti vuota</td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }
}

?>