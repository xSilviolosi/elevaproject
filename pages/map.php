<?php

class createPage {

    static function page() {
        global $mysqli;
        ?>
        <div class="row">
            <div class="col-12">
                <h1>La mappa</h1>
                <hr>
                <div id="map" data-section="map"></div>
            </div>
        </div>
        <div id="sidebar">
            <span class="close-sidebar">X</span>
            <div class="info"></div>
        </div>
        <?php
    }

}

?>