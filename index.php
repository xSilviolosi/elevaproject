<?php

// Load classes
require dirname(__FILE__) . "/includes/_load.php";
// Get page
$page = (!empty($_GET['p'])) ? $_GET['p'] : "home";
// Build page
Template::header();
Template::page($page);
Template::footer();