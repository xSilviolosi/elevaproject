<?php

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

ob_start();

// Load classes
function autoload_class($class) {
    $regPath = dirname(__FILE__) . "/" . $class . ".class.php";
    if (file_exists($regPath)) {
        include $regPath;
    }
}

spl_autoload_register("autoload_class");

// Initialize MySql connection with MySqli
if (file_exists(dirname(__FILE__) . "/Mysql.class.php") &&
    (
        !empty(Mysql::DB_HOST) ||
        !empty(Mysql::DB_USERNAME) ||
        !empty(Mysql::DB_PASSWORD) ||
        !empty(Mysql::DB_DATABASE)
    )
) {
    $mysqli = new mysqli(Mysql::DB_HOST, Mysql::DB_USERNAME, Mysql::DB_PASSWORD, Mysql::DB_DATABASE);
    if ($mysqli->connect_error) {
        die("Connection error (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
    } else {
        $mysqli->set_charset("utf8");
    }
}

// General constants
define("SITE_TITLE", "");
define("SITE_PATH",Utility::get_path());
// Path constants
define("PAGE_PATH", "pages/");
define("JS_PATH", "assets/js/");
define("CSS_PATH", "assets/css/");