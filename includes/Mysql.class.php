<?php

class Mysql {

	// DB Parameters
	const DB_HOST = "localhost";
	const DB_USERNAME = "root";
	const DB_PASSWORD = "root";
	const DB_DATABASE = "elevaproject";

	// Method to add new user
	public static function addUser() {
		global $mysqli;
		if ( ( $_POST ) ) {
			extract( $_POST );
			$query = $mysqli->prepare( "INSERT INTO users(name, surname, email, birthdate, role) VALUES(?,?,?,?,?)" );
			$query->bind_param( "sssss", $name, $surname, $email, $datepicker, $role );
			$result = $query->execute() or die( $mysqli->error );
			$user_id = $mysqli->insert_id;
			if ( isset( $structures ) && $structures != "" ) {
				$sql = "";
				foreach ( $structures as $structure ) {
					$sql .= "INSERT INTO user_structures(user_id, structure_id) VALUES($user_id, $structure);";
				}
				$mysqli->multi_query( $sql ) or die( $mysqli->error );
			}
			if ( $result == true ) {
				header( "Location: " . SITE_PATH . "users.html?section=user&method=add&msg=success" );
			} else {
				header( "Location: " . SITE_PATH . "users.html?section=user&method=add&msg=failed" );
			}
			$mysqli->close();
		}
	}

	// Method to edit a user
	public static function editUser( $idUser ) {
		global $mysqli;
		if ( ( $_POST ) ) {
			extract( $_POST );
			$query = $mysqli->prepare( "UPDATE users SET name = ?, surname = ?, email = ?, birthdate = ?, role = ? WHERE user_id = ?" );
			$query->bind_param( "sssssi", $name, $surname, $email, $datepicker, $role, $idUser );
			$result = $query->execute() or die( $mysqli->error );
			if ( isset( $structures ) && $structures != "" ) {
				$query = $mysqli->prepare( "DELETE FROM user_structures WHERE user_id = ?" );
				$query->bind_param( "i", $idUser );
				$result = $query->execute() or die( $mysqli->error );
				$sql = "";
				foreach ( $structures as $structure ) {
					$sql .= "INSERT INTO user_structures(user_id, structure_id) VALUES($idUser, $structure);";
				}
				$mysqli->multi_query( $sql ) or die( $mysqli->error );
			}
			if ( $result == true ) {
				header( "Location: " . SITE_PATH . "users.html?section=user&method=edit&msg=success" );
			} else {
				header( "Location: " . SITE_PATH . "users.html?section=user&method=edit&msg=failed" );
			}
			$mysqli->close();
		}
	}

	// Method to add new structure
	public static function addStructure() {
		global $mysqli;
		if ( ( $_POST ) ) {
			extract( $_POST );
			$query = $mysqli->prepare( "INSERT INTO structures(name, address, lat, lon) VALUES(?,?,?,?)" );
			$query->bind_param( "ssss", $name, $address, $lat, $lon );
			$result = $query->execute() or die( $mysqli->error );
			if ( $result == true ) {
				header( "Location: " . SITE_PATH . "structures.html?section=structure&method=add&msg=success" );
			} else {
				header( "Location: " . SITE_PATH . "structures.html?section=structure&method=add&msg=failed" );
			}
			$mysqli->close();
		}
	}

	// Method to edit a structure
	public static function editStructure( $idStructure ) {
		global $mysqli;
		if ( ( $_POST ) ) {
			extract( $_POST );
			$query = $mysqli->prepare( "UPDATE structures SET name = ?, address = ?, lat = ?, lon = ? WHERE structure_id = ?" );
			$query->bind_param( "ssssi", $name, $address, $lat, $lon, $idStructure );
			$result = $query->execute() or die( $mysqli->error );
			if ( $result == true ) {
				header( "Location: " . SITE_PATH . "structures.html?section=structure&method=edit&msg=success" );
			} else {
				header( "Location: " . SITE_PATH . "structures.html?section=structure&method=edit&msg=failed" );
			}
			$mysqli->close();
		}
	}

	// Method to get a row from DB
	public static function get_row( $obj, $tab, $cond, $val, $add ) {
		global $mysqli;
		$ret = "";
		$sql = "SELECT " . $obj . " FROM " . $tab . " WHERE " . $cond . " = '" . $val . "'" . $add;
		$result = $mysqli->query( $sql ) or die( $mysqli->error );
		if ( $row = $result->fetch_assoc() ) {
			$ret = $row;
		}

		return $ret;
	}

	// Method to get structures per user
	public static function get_users( $idStructures ) {
		global $mysqli;
		$ret = [];
		$sql = "SELECT DISTINCT u.name, u.surname, u.role FROM users u
				LEFT JOIN user_structures us
				on us.user_id = u.user_id
				WHERE us.structure_id = $idStructures
				ORDER BY 3 DESC";
		$result = $mysqli->query( $sql ) or die( $mysqli->error );
		if ( mysqli_num_rows( $result ) > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				array_push( $ret, $row );
			}
		}

		return $ret;
	}

	// Method to get structures. idUser is optional
	public static function get_structures( $idUser = false ) {
		global $mysqli;
		$ret = [];
		$sql = "SELECT s.structure_id, s.name, s.address, s.lat, s.lon FROM structures s 
				LEFT JOIN user_structures us
				ON us.structure_id = s.structure_id";
		if ( $idUser ) {
			$sql .= " WHERE us.user_id = $idUser";
		}
		$result = $mysqli->query( $sql ) or die( $mysqli->error );
		if ( mysqli_num_rows( $result ) > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				$row['users'] = [];
				$users        = self::get_users( $row['structure_id'] );
				array_push( $row['users'], $users );
				array_push( $ret, $row );
			}
		}

		return $ret;
	}

	// Method to create DB and Tables
	public static function createDB() {
		// Create database
		global $mysqli;
		$msg = "";
		if ( Mysql::DB_HOST && Mysql::DB_USERNAME && Mysql::DB_PASSWORD && Mysql::DB_DATABASE ) {
			$msg .= "Creazione database <b>" . Mysql::DB_DATABASE . "</b>:<br />";
			//$resultDB = $mysqli->query( "DROP DATABASE elevaproject" ) or die( $mysqli->error );
			$resultDB = $mysqli->query( "CREATE DATABASE IF NOT EXISTS " . Mysql::DB_DATABASE ) or die( $mysqli->error );
			if ( $resultDB === true ) {
				$msg .= "Il database <b>elevaproject</b> &egrave; stato creato!<br /><br />";
				$msg .= "Creazione tabella <b>users</b>:<br />";
				$msg .= "... Controllo che non esista gi&agrave;:<br />";
				$result = $mysqli->query( "SHOW TABLES LIKE 'users'" ) or die ( $mysqli->error );
				if ( $result->num_rows == 0 ) {
					$msg .= "... Non esiste, creo la tabella...<br />";
					$resultTBUser = $mysqli->query( "
			    CREATE TABLE users (
			    user_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
			    name VARCHAR(255),
			    surname VARCHAR(255),
			    email VARCHAR(255),
			    birthdate VARCHAR(255),
			    role VARCHAR(255)
			    )" ) or die( $mysqli->error );
					if ( $resultTBUser === true ) {
						$msg .= "Tabella <b>users</b> creata!<br /><br />";
					} else {
						$msg .= "Errore nella creazione della tabella <b>users</b>!<br /><br />";
					}
				} else {
					$msg .= "La tabella <b>users</b> esiste gi&agrave;.<br /><br />";
				}
				$msg .= "Creazione tabella <b>structures</b>:<br />";
				$msg .= "... Controllo che non esista gi&agrave;:<br />";
				$result = $mysqli->query( "SHOW TABLES LIKE 'structures'" ) or die ( $mysqli->error );
				if ( $result->num_rows == 0 ) {
					$msg .= "... Non esiste, creo la tabella...<br />";
					$resultTBStructure = $mysqli->query( "
				CREATE TABLE structures (
				structure_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
				name VARCHAR(255),
				address VARCHAR(255),
				lat VARCHAR(255),
				lon VARCHAR(255)
			    )" ) or die( $mysqli->error );
					if ( $resultTBStructure === true ) {
						$msg .= "Tabella <b>structures</b> creata!<br />";
					} else {
						$msg .= "Errore nella creazione della tabella <b>structures</b>.<br /><br />";
					}
				} else {
					$msg .= "La tabella <b>structures</b> esiste gi&agrave;.<br /><br />";
				}
				$msg .= "Creazione tabella <b>user_structures</b>:<br />";
				$msg .= "... Controllo che non esista gi&agrave;:<br />";
				$result = $mysqli->query( "SHOW TABLES LIKE 'user_structures'" ) or die ( $mysqli->error );
				if ( $result->num_rows == 0 ) {
					$msg .= "... Non esiste, creo la tabella...<br />";
					$resultTBUserStrct = $mysqli->query( "
			    CREATE TABLE user_structures (
			    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
			    user_id INT UNSIGNED NOT NULL,
			    structure_id INT UNSIGNED NOT NULL
			    )" ) or die( $mysqli->error );
					if ( $resultTBUserStrct === true ) {
						$msg .= "Tabella <b>user_structures</b> creata!<br /><br />";
					} else {
						$msg .= "Errore nella creazione della tabella <b>user_structures</b>!<br /><br />";
					}
				} else {
					$msg .= "La tabella <b>user_structures</b> esiste gi&agrave;.<br /><br />";
				}
			} else {
				$msg .= "Errore nella creazione del DB <b>elevaproject</b>.<br />";
			}
		} else {
			$msg = "Creazione database fallita. Controllare le configurazioni del database sul file includes/Mysql.class.php e riprovare.<br />";
		}
		return $msg;
	}

}
