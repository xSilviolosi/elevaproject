<?php

class Template {

    public static function header() {
    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= SITE_TITLE ?></title>
        <?php self::css_scripts(); ?>
    </head>
    <body>
    <?php self::nav();
    }

    public static function page($page) {
        $include_page = PAGE_PATH . "$page.php";
        include $include_page;
        ?>
        <div class="container-fluid">
            <?php createPage::page(); ?>
        </div>
        <?php
    }

    private static function nav() {
        ?>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.html">Eleva project</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="home.html">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="users.html">Utenti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="structures.html">Impianti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="map.html">Mappa</a>
                    </li>
                </ul>
            </div>
        </nav>
        <?php
    }

    public static function footer() {
    ?>
    <?php self::js_scripts(); ?>
    </body>
    </html>
    <?php
}

    private static function js_scripts() {
        ?>
        <script src="<?= JS_PATH ?>jquery.min.js"></script>
        <script src="<?= JS_PATH ?>bootstrap.min.js"></script>
        <script src="<?= JS_PATH ?>jquery-ui.min.js"></script>
        <script src="<?= JS_PATH ?>i18n/datepicker-it.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9lqYdLCfSnDnXYodKi6mrtUwV8ftccwc&libraries=places"></script>
        <script src="<?= JS_PATH ?>scripts.js"></script>
        <?php
    }

    private static function css_scripts() {
        ?>
        <link rel="stylesheet" href="<?= CSS_PATH ?>bootstrap.min.css">
        <link rel="stylesheet" href="<?= CSS_PATH ?>jquery-ui.min.css">
        <link rel="stylesheet" href="<?= CSS_PATH ?>style.css">
        <?php
    }

}

?>