<?php

class Utility {

	// Method to get basic path
	static function get_path() {
		$url = $_SERVER['SERVER_NAME'];
		switch ( $url ) {
			case "localhost":
				$url = $_SERVER['SCRIPT_NAME'];
				$url = explode( "/", $url );
				$url = "/" . $url[1] . "/";
				break;
			default:
				$base_url = ( ( isset( $_SERVER['HTTPS'] ) ) && ( $_SERVER['HTTPS'] == "on" ) ) ? "https://" : "http://";
				$url      = $base_url . $url . "/";
				break;
		}
		return $url;
	}

	// Method to get alert box
	static function getAlertMsg( $section, $method, $msg ) {
		?>
        <div class="alert <?= ( $msg == "success" ) ? "alert-success" : "alert-danger"; ?>" role="alert">
			<?php
			switch ( $section ) {
				case "user":
					switch ( $method ) {
						case "add":
							switch ( $msg ) {
								case "success":
									echo "Personale inserito con successo!";
									break;
								case "failed":
									echo "Inserimento non avvenuto correttamente. Riprovare.";
									break;
							}
							break;
						case "edit":
							switch ( $msg ) {
								case "success":
									echo "Personale modificato correttamente!";
									break;
								case "failed":
									echo "Modifica non avvenuta. Riprovare.";
									break;
							}
							break;
					}
					break;
				case "structure":
					switch ( $method ) {
						case "add":
							switch ( $msg ) {
								case "success":
									echo "Struttura inserita con successo!";
									break;
								case "failed":
									echo "Inserimento non avvenuto correttamente. Riprovare.";
									break;
							}
							break;
						case "edit":
							switch ( $msg ) {
								case "success":
									echo "Struttura modificata correttamente!";
									break;
								case "failed":
									echo "Modifica non avvenuta. Riprovare.";
									break;
							}
							break;
					}
					break;
			}
			?>
        </div>
		<?php
	}

}

?>