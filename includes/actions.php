<?php

require "_load.php";
if (isset($_GET)) {
    extract($_GET);
    switch ($section) {
        case "user":
            switch ($method) {
                case "add":
                    Mysql::addUser();
                    break;
                case "edit":
                    Mysql::editUser($idUser);
                    break;
                case "loadUser":
                    $user = Mysql::get_row("*", "users", "user_id", $idUser, false);
                    $structures = Mysql::get_structures($idUser);
                    $user['structures'] = [];
                    array_push($user['structures'], $structures);
					echo json_encode($user);
					break;
            }
            break;
        case "structure":
            switch ($method) {
                case "add":
                    Mysql::addStructure();
                    break;
                case "edit":
                    Mysql::editStructure($idStructure);
                    break;
                case "loadStructure":
                    $structure = Mysql::get_row("*", "structures", "structure_id", $idStructure, false);
                    echo json_encode($structure);
                    break;
            }
            break;
	    case "map":
	    	switch ($method) {
			    case "loadStructures":
				    $structures = Mysql::get_structures();
				    echo json_encode($structures);
				    break;
		    }
	    	break;
        case "other":
            switch ($method) {
                case "createDB":
                    echo Mysql::createDB();
                    break;
            }
            break;
    }
}

?>